const gulp          = require('gulp');
const autoprefixer  = require('autoprefixer');
const sourcemaps    = require('gulp-sourcemaps');
const del 					= require('del');
const $             = require('gulp-load-plugins')();
const exec 					= require('child_process').exec;

function sass() {
	return gulp.src('scss/app.scss')
		.pipe(sourcemaps.init())
		.pipe($.sass({
			outputStyle: 'compact'
		})
			.on('error', $.sass.logError))
		.pipe($.postcss([
			autoprefixer({ overrideBrowserslist: ['last 2 versions', 'ie >= 9'] })
		]))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('css'));
};

function kssbase() {
	return gulp.src('scss/kssbase/*.scss')
		.pipe(sourcemaps.init())
		.pipe($.sass({
			outputStyle: 'compact'
		})
			.on('error', $.sass.logError))
		.pipe($.postcss([
			autoprefixer({ overrideBrowserslist: ['last 2 versions', 'ie >= 9'] })
		]))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('css/kssbase'));
}

function atoms() {
	return gulp.src('scss/atoms/**/*.scss')
		.pipe(sourcemaps.init())
		.pipe($.sass({
			outputStyle: 'compact'
		})
			.on('error', $.sass.logError))
		.pipe($.postcss([
			autoprefixer({ overrideBrowserslist: ['last 2 versions', 'ie >= 9'] })
		]))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('css/atoms'));
};

function molecules() {
	return gulp.src('scss/molecules/**/*.scss')
		.pipe(sourcemaps.init())
		.pipe($.sass({
			outputStyle: 'compact'
		})
			.on('error', $.sass.logError))
		.pipe($.postcss([
			autoprefixer({ overrideBrowserslist: ['last 2 versions', 'ie >= 9'] })
		]))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('css/molecules'));
};

function kss(cb) {
	exec('node node_modules/kss/bin/kss --config kss-scheibo.json', function(err) {
		cb(err);
	});
}

// update foundation dependencies js (needs to be listed here)
var jsPaths = [
	'node_modules/foundation-sites/dist/js/plugins/foundation.core.min.js',
	'node_modules/foundation-sites/dist/js/plugins/foundation.core.min.js.map',
	'node_modules/foundation-sites/dist/js/plugins/foundation.util.keyboard.min.js',
	'node_modules/foundation-sites/dist/js/plugins/foundation.util.keyboard.min.js.map',
	'node_modules/foundation-sites/dist/js/plugins/foundation.accordion.min.js',
	'node_modules/foundation-sites/dist/js/plugins/foundation.accordion.min.js.map'
];
gulp.task('copyjs', function () {
  return gulp.src(jsPaths).pipe(gulp.dest('js'));
});

gulp.task('sass', sass);
gulp.task('watch', () => {
	gulp.watch('./scss/**/*.scss', gulp.parallel(sass, atoms, molecules, kssbase, kss));
});
gulp.task('clean', function(){
	return del('./css/**', {force:true});
});