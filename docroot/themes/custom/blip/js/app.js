/**
 * @file
 * Global utilities.
 *
 */
(function($, Drupal) {

	Drupal.behaviors.foundation_init = {
		attach: function(context, settings) {

			$(document).foundation();

		}
	}

  Drupal.behaviors.burger_menu = {
    attach: function(context, settings) {

      // variables
      var burgerToggler = $('.burger-toggler'),
          mainNav = $('#block-blip-mainnavigation'),
          mobileParentToggle = mainNav.find('.is-parent > sub');

      // on burger toggle
			burgerToggler.on('click', function() {
				if (burgerToggler.hasClass('is-expanded')) {
					burgerToggler.removeClass('is-expanded');
					mainNav.removeClass('is-expanded');
				} else {
					burgerToggler.addClass('is-expanded');
					mainNav.addClass('is-expanded');
				}
      });
      
      // on navigation parent click
			mobileParentToggle.on('click', function() {
				var _self = $(this);

				if (_self.parent('.is-parent').hasClass('is-expanded')) {
					_self.parent('.is-parent').removeClass('is-expanded');
				} else {
					_self.parent('.is-parent').addClass('is-expanded');
				}
			});

    }
  }

})(jQuery, Drupal);