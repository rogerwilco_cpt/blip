<?php

namespace Drupal\rw_pagebuilder\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\FileSystem\FileSystem;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Url;

/**
 * Builds the form to delete Page Mode presets.
 */
class PageModeDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.pagemode.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $path = 'public://pagemodes/' . $this->entity->id() . '/' . $this->entity->id() . '.theme.css';
//    \Drupal::service('file_system')->delete($path);
//    \Drupal::service('file_system')->rmdir('public://pagemodes/' . $this->entity->id());
    \Drupal::service('file_system')->deleteRecursive('public://pagemodes/' . $this->entity->id());

    \Drupal::messenger()->addStatus(
      $this->t('@file has been deleted',
      [
        '@file' => $path,
      ])
    );

    $this->entity->delete();

    \Drupal::messenger()->addStatus(
      $this->t('@type preset: deleted @label',
        [
          '@type'  => $this->entity->bundle(),
          '@label' => $this->entity->label(),
        ]
      )
    );

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
