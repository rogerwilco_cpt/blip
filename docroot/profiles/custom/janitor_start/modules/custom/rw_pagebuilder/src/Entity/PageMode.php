<?php

namespace Drupal\rw_pagebuilder\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Page Mode preset entity.
 *
 * @ConfigEntityType(
 *   id = "pagemode",
 *   label = @Translation("Page Mode preset"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\rw_pagebuilder\PageModeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\rw_pagebuilder\Form\PageModeForm",
 *       "edit" = "Drupal\rw_pagebuilder\Form\PageModeForm",
 *       "delete" = "Drupal\rw_pagebuilder\Form\PageModeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\rw_pagebuilder\PageModeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "pagemode",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/appearance/pagemode/{pagemode}",
 *     "add-form" = "/admin/appearance/pagemode/add",
 *     "edit-form" = "/admin/appearance/pagemode/{pagemode}/edit",
 *     "delete-form" = "/admin/appearance/pagemode/{pagemode}/delete",
 *     "collection" = "/admin/appearance/pagemode"
 *   }
 * )
 */
class PageMode extends ConfigEntityBase implements PageModeInterface {

  /**
   * The Page Mode preset ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Page Mode preset label.
   *
   * @var string
   */
  protected $label;

  /**
   * Enable Page Mode
   *
   * @var boolean
   */
  public $enable_pagemode;

  /**
   * Enable CSS custom properties
   *
   * @var boolean
   */
  public $enable_custom_properties;

  /**
   * (body) Background color
   *
   * @var string
   */
  public $color_background;

  /**
   * (body) Text color
   *
   * @var string
   */
  public $color_body_copy;

  /**
   * Primary color
   *
   * @var string
   */
  public $color_primary;

  /**
   * Secondary color
   *
   * @var string
   */
  public $color_secondary;

  /**
   * Highlight color
   *
   * @var string
   */
  public $color_highlight;

  /**
   * Link color
   *
   * @var string
   */
  public $color_link;

  /**
   * Gutter width
   *
   * @var integer
   */
  public $gutter_value;

  /**
   * Gutter unit (default 'px')
   *
   * @var string
   */
  public $gutter_unit;
}
