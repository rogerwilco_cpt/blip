<?php

namespace Drupal\rw_pagebuilder\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Page Mode presets.
 */
interface PageModeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
