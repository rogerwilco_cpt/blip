<?php

namespace Drupal\rw_pagebuilder;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Page Mode presets.
 */
class PageModeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['active']            = $this->t('Active');
    $header['label']             = $this->t('Page Mode');
    // $header['id']                = $this->t('Machine name');
    $header['custom_properties'] = $this->t('Custom properties');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    // kint($entity);

    if ($entity->get('enable_custom_properties')) {
      $palette = [
        '#theme'            => 'pagemode_palette',
        '#id'               => $entity->id(),
        '#color_background' => $entity->color_background,
        '#color_body_copy'  => $entity->color_body_copy,
        '#color_primary'    => $entity->color_primary,
        '#color_secondary'  => $entity->color_secondary,
        '#color_highlight'  => $entity->color_highlight,
        '#color_link'       => $entity->color_link,
        '#gutter'           => $entity->gutter_value . $entity->gutter_unit,
      ];

      $custom_properties = render($palette);
    }
    else {
      $custom_properties = ''; // $this->t('Disabled');
    }

    // @TODO: Make the listbuilder be a form that supports bulk publish/enable
    //        pagemode features
    $row['active'] = ($entity->get('enable_pagemode')) ? '✅' : '✖️';
    $row['label']  = $entity->label() . ' (' . $entity->id() . ')';
    // $row['id']  = $entity->id();
    $row['custom_properties'] = $custom_properties;
    return $row + parent::buildRow($entity);
  }

}
