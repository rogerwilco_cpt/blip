<?php

namespace Drupal\rw_pagebuilder\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Color;
use Drupal\file\Entity\File;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Component\Utility\Number;

/**
 * Class PageModeForm.
 */
class PageModeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $pagemode = $this->entity;
    $form['label'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Preset name'),
      '#maxlength'     => 255,
      '#default_value' => $pagemode->label(),
      '#description'   => $this->t("Provide a name for this set of Page Mode presets. The machine name will be used for generating custom theme folder and templates, etc."),
      '#required'      => TRUE,
    ];

    $form['id'] = [
      '#type'          => 'machine_name',
      '#default_value' => $pagemode->id(),
      '#machine_name'  => [
        'exists'       => '\Drupal\rw_pagebuilder\Entity\PageMode::load',
      ],
      '#disabled'      => !$pagemode->isNew(),
    ];

    $form['enable_pagemode'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Active'),
      '#default_value' => $pagemode->enable_pagemode,
      '#description'   => $this->t('Check this box if you want to enable Page Mode on nodes using this preset.'),
      '#required'      => FALSE,
    ];

    // @TODO: Use states to show/hide the custom property fields based on this
    //        checkbox
    $form['enable_custom_properties'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enable custom properties'),
      '#default_value' => $pagemode->enable_custom_properties,
      '#description'   => $this->t(
        'Check this box if you would like Page Modes to provide some default  CSS custom properties you could integrate into your theme.',
      [
        // '@custom_properties_link' => '<a href="https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties">CSS custom properties</a>',
      ]),
      '#required'      => FALSE,
    ];

    $form['color_palette'] = [
      '#type'          => 'details',
      '#title'         => $this->t('Color palette'),
      '#open'          => TRUE,
      '#states'        => [
        'visible'      => [
          ':input[name="enable_custom_properties"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['gutter_group'] = [
      '#type'          => 'details',
      '#title'         => $this->t('Gutter'),
      '#open'          => TRUE,
      '#states'        => [
        'visible'      => [
          ':input[name="enable_custom_properties"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['color_background'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Background color'),
      '#field_prefix'  => '#',
      '#default_value' => $pagemode->color_background,
      '#placeholder'   => t('E.g. fff'),
      '#description'   => $this->t('Provide a hexadecimal value for a color.'),
      '#maxlength'     => 8,
      '#size'          => 12,
      '#group'         => 'color_palette',
    ];

    $form['color_body_copy'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Text color'),
      '#field_prefix'  => '#',
      '#default_value' => $pagemode->color_body_copy,
      '#placeholder'   => t('E.g. 444'),
      '#description'   => $this->t('Provide a hexadecimal value for a color.'),
      '#maxlength'     => 8,
      '#size'          => 12,
      '#group'         => 'color_palette',
    ];

    $form['color_primary'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Primary color'),
      '#field_prefix'  => '#',
      '#default_value' => $pagemode->color_primary,
      '#placeholder'   => t('E.g. 0678be'),
      '#description'   => $this->t('Provide a hexadecimal value for the main color.'),
      '#maxlength'     => 8,
      '#size'          => 12,
      '#group'         => 'color_palette',
    ];

    $form['color_secondary'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Secondary color'),
      '#field_prefix'  => '#',
      '#default_value' => $pagemode->color_secondary,
      '#placeholder'   => t('E.g. 064771'),
      '#description'   => $this->t('Provide a hexadecimal value for the supporting color.'),
      '#maxlength'     => 8,
      '#size'          => 12,
      '#group'         => 'color_palette',
    ];

    $form['color_highlight'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Highlight color'),
      '#field_prefix'  => '#',
      '#default_value' => $pagemode->color_highlight,
      '#placeholder'   => t('E.g. 7cbc48'),
      '#description'   => $this->t('Provide a hexadecimal value for the contrasting/CTA color.'),
      '#maxlength'     => 8,
      '#size'          => 12,
      '#group'         => 'color_palette',
    ];

    $form['color_link'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Link color'),
      '#field_prefix'  => '#',
      '#default_value' => $pagemode->color_link,
      '#placeholder'   => t('E.g. 0000ee'),
      '#description'   => $this->t('Provide a hexadecimal value hyperlinks. '),
      '#maxlength'     => 8,
      '#size'          => 12,
      '#group'         => 'color_palette',
    ];

    $form['gutter_value'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Gutter value'),
      '#default_value' => $pagemode->gutter_value,
      '#placeholder'   => t('E.g. 32'),
      '#description'   => $this->t('Provide an integer value to form a default gap or space between elements. Use multiples of this value to create vertical rhythm'),
      '#attributes'    => [
        ' type'        => 'number',
      ],
      '#min'           => 0,
      '#size'          => 6,
      '#maxlength'     => 3,
      '#group'         => 'gutter_group',
    ];

    $form['gutter_unit'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Gutter unit'),
      '#default_value' => (isset($pagemode->gutter_unit) && !empty($pagemode->gutter_unit)) ? $pagemode->gutter_unit : 'px',
      '#description'   => $this->t('Select the unit of measurement to apply to the gutter value, e.g. pixels (default)'),
      '#options'       => [
        'px'           => 'px',
        '%'            => '%',
        'vw'           => 'vw',
        'vh'           => 'vh',
        'rem'          => 'rem',
        'em'           => 'em',
      ],
      '#group' => 'gutter_group',
      '#access'        => FALSE, // @TOOO: Fix the group display thingy
    ];

    return $form;
  }

  // @TODO: Validate Hexadecimal values for colors
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    if ($values['enable_custom_properties']) {
      // @TODO: Feels like I can abstract this a little better
      // Background color
      if (!Color::validateHex($values['color_background'])) {
        $form_state
          ->setErrorByName('color_background', $this
          ->t('Please supply a valid hexadecimal color code'));
      }

      // Body copy color
      if (!Color::validateHex($values['color_body_copy'])) {
        $form_state
          ->setErrorByName('color_body_copy', $this
          ->t('Please supply a valid hexadecimal color code'));
      }

      // Primary color
      if (!Color::validateHex($values['color_primary'])) {
        $form_state
          ->setErrorByName('color_primary', $this
          ->t('Please supply a valid hexadecimal color code'));
      }

      // Secondary color
      if (!Color::validateHex($values['color_secondary'])) {
        $form_state
          ->setErrorByName('color_secondary', $this
          ->t('Please supply a valid hexadecimal color code'));
      }

      // Highlight color
      if (!Color::validateHex($values['color_highlight'])) {
        $form_state
          ->setErrorByName('color_highlight', $this
          ->t('Please supply a valid hexadecimal color code'));
      }

      // Link color
      if (!Color::validateHex($values['color_link'])) {
        $form_state
          ->setErrorByName('color_link', $this
          ->t('Please supply a valid hexadecimal color code'));
      }
    }

    parent::validateForm($form, $form_state); // TODO: Change the autogenerated stub
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $pagemode = $this->entity;
    $status   = $pagemode->save();

    switch ($status) {
      case SAVED_NEW:
        \Drupal::messenger()->addStatus(
          $this->t('Created the %label Page Mode preset.', [
            '%label' => $pagemode->label(),
          ])
        );
        break;

      default:
        \Drupal::messenger()->addStatus(
          $this->t('Saved the %label Page Mode preset.', [
            '%label' => $pagemode->label(),
          ])
        );
        break;
    }

    // Create folder(s) for the default Page Mode CSS to be extended or
    // overridden in the theme.
    // @TODO: Make this a method on the entity class
    $path = 'public://pagemodes/' . $pagemode->id() . '/';

    \Drupal::service('file_system')->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);

    $file_content  = "/** \n";
    $file_content .= " * @file \n";
    $file_content .= " * Default styles for the '" . $pagemode->label() . "' (" . $pagemode->id() . ") Page Mode preset \n";
    $file_content .= " */ \n";

    // If custom properties are enabled, add the base CSS to add variables to
    // <body> tag
    if ($pagemode->get('enable_custom_properties')) {
      $file_content .= "\n";

      $file_content .=  "body.pagemode--" . $pagemode->id() . " {\n";
      $file_content .= "  --rw-primary-color: #" . $pagemode->get('color_primary') . ";\n";
      $file_content .= "  --rw-secondary-color: #" . $pagemode->get('color_secondary') . ";\n";
      $file_content .= "  --rw-highlight-color: #" . $pagemode->get('color_highlight') . ";\n";
      $file_content .= "  --rw-background-color: #" . $pagemode->get('color_background') . ";\n";
      $file_content .= "  --rw-text-color: #" . $pagemode->get('color_body_copy') . ";\n";
      $file_content .= "  --rw-link-color: #" . $pagemode->get('color_link') . ";\n";
      $file_content .= "  --rw-gutter: " . $pagemode->get('gutter_value') . $pagemode->get('gutter_unit') . ";\n";
      $file_content .= "  background-color: var(--rw-background-color);\n";
      $file_content .= "  color: var(--rw-text-color);\n";
      $file_content .= "}\n";
      $file_content .= "a {\n";
      $file_content .= "  color: var(--rw-link-color, #" . $pagemode->get('color_link') . ");\n";
      $file_content .= "}\n";
    }

    $destination = "public://pagemodes/" . $pagemode->id() . "/" . $pagemode->id() . ".theme.css";

    $css_file = file_save_data(
      $file_content,
      $destination,
      FileSystemInterface::EXISTS_REPLACE
    );

    if (is_object($css_file)) {
      \Drupal::messenger()->addStatus($this->t('CSS file created in @destination', ['@destination' => $destination]));
    }
    else {
      \Drupal::messenger()->addError($this->t('Could not create CSS file for @destination', ['@destination' => $destination]));
    }

    $form_state->setRedirectUrl($pagemode->toUrl('collection'));
  }


}
