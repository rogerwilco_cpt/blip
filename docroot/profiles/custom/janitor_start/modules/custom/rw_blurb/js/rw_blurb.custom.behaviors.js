/**
 * @file
 * Provides an empty behaviours file for theme's and other modules to override
 * for custom animations
 */
(function ($) {
  Drupal.behaviors.blurbRow = {
    attach: function (context) {
      console.log('Page builder: Blurb row custom behaviours added. Use libraries override in your theme to override rw_blurb/custom');
    }
  };
})(jQuery);
