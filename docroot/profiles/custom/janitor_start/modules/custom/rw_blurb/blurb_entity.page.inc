<?php

/**
 * @file
 * Contains blurb_entity.page.inc.
 *
 * Page callback for Blurb entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Blurb entity templates.
 *
 * Default template: blurb_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_blurb_entity(array &$variables) {
  // Fetch blurbEntity Entity Object.
  $blurb_entity = $variables['elements']['#blurb_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
