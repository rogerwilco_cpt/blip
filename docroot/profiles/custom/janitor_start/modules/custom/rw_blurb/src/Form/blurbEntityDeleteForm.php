<?php

namespace Drupal\rw_blurb\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Blurb entity entities.
 *
 * @ingroup rw_blurb
 */
class blurbEntityDeleteForm extends ContentEntityDeleteForm {


}
