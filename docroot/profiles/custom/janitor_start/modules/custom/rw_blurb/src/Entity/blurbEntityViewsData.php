<?php

namespace Drupal\rw_blurb\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Blurb entity entities.
 */
class blurbEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
