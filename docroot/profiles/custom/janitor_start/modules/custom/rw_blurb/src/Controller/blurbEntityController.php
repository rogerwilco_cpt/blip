<?php

namespace Drupal\rw_blurb\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Drupal\rw_blurb\Entity\blurbEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class blurbEntityController.
 *
 *  Returns responses for Blurb entity routes.
 */
class blurbEntityController extends ControllerBase implements ContainerInjectionInterface {


  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Constructs a new blurbEntityController.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct(DateFormatter $date_formatter, Renderer $renderer) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Displays a Blurb entity revision.
   *
   * @param int $blurb_entity_revision
   *   The Blurb entity revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($blurb_entity_revision) {
    $blurb_entity = $this->entityTypeManager()->getStorage('blurb_entity')
      ->loadRevision($blurb_entity_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('blurb_entity');

    return $view_builder->view($blurb_entity);
  }

  /**
   * Page title callback for a Blurb entity revision.
   *
   * @param int $blurb_entity_revision
   *   The Blurb entity revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($blurb_entity_revision) {
    $blurb_entity = $this->entityTypeManager()->getStorage('blurb_entity')
      ->loadRevision($blurb_entity_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $blurb_entity->label(),
      '%date' => $this->dateFormatter->format($blurb_entity->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Blurb entity.
   *
   * @param \Drupal\rw_blurb\Entity\blurbEntityInterface $blurb_entity
   *   A Blurb entity object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(blurbEntityInterface $blurb_entity) {
    $account = $this->currentUser();
    $blurb_entity_storage = $this->entityTypeManager()->getStorage('blurb_entity');

    $langcode = $blurb_entity->language()->getId();
    $langname = $blurb_entity->language()->getName();
    $languages = $blurb_entity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $blurb_entity->label()]) : $this->t('Revisions for %title', ['%title' => $blurb_entity->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all blurb entity revisions") || $account->hasPermission('administer blurb entity entities')));
    $delete_permission = (($account->hasPermission("delete all blurb entity revisions") || $account->hasPermission('administer blurb entity entities')));

    $rows = [];

    $vids = $blurb_entity_storage->revisionIds($blurb_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\rw_blurb\blurbEntityInterface $revision */
      $revision = $blurb_entity_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $blurb_entity->getRevisionId()) {
          $link = $this->l($date, new Url('entity.blurb_entity.revision', [
            'blurb_entity' => $blurb_entity->id(),
            'blurb_entity_revision' => $vid,
          ]));
        }
        else {
          $link = $blurb_entity->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.blurb_entity.translation_revert', [
                'blurb_entity' => $blurb_entity->id(),
                'blurb_entity_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.blurb_entity.revision_revert', [
                'blurb_entity' => $blurb_entity->id(),
                'blurb_entity_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.blurb_entity.revision_delete', [
                'blurb_entity' => $blurb_entity->id(),
                'blurb_entity_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['blurb_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
