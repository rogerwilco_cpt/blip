<?php

namespace Drupal\rw_blurb;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\rw_blurb\Entity\blurbEntityInterface;

/**
 * Defines the storage handler class for Blurb entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Blurb entity entities.
 *
 * @ingroup rw_blurb
 */
interface blurbEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Blurb entity revision IDs for a specific Blurb entity.
   *
   * @param \Drupal\rw_blurb\Entity\blurbEntityInterface $entity
   *   The Blurb entity entity.
   *
   * @return int[]
   *   Blurb entity revision IDs (in ascending order).
   */
  public function revisionIds(blurbEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Blurb entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Blurb entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\rw_blurb\Entity\blurbEntityInterface $entity
   *   The Blurb entity entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(blurbEntityInterface $entity);

  /**
   * Unsets the language for all Blurb entity with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
