<?php

namespace Drupal\rw_blurb\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Blurb entity entities.
 *
 * @ingroup rw_blurb
 */
interface blurbEntityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Blurb entity name.
   *
   * @return string
   *   Name of the Blurb entity.
   */
  public function getName();

  /**
   * Sets the Blurb entity name.
   *
   * @param string $name
   *   The Blurb entity name.
   *
   * @return \Drupal\rw_blurb\Entity\blurbEntityInterface
   *   The called Blurb entity entity.
   */
  public function setName($name);

  /**
   * Gets the Blurb entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Blurb entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Blurb entity creation timestamp.
   *
   * @param int $timestamp
   *   The Blurb entity creation timestamp.
   *
   * @return \Drupal\rw_blurb\Entity\blurbEntityInterface
   *   The called Blurb entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Blurb entity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Blurb entity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\rw_blurb\Entity\blurbEntityInterface
   *   The called Blurb entity entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Blurb entity revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Blurb entity revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\rw_blurb\Entity\blurbEntityInterface
   *   The called Blurb entity entity.
   */
  public function setRevisionUserId($uid);

}
