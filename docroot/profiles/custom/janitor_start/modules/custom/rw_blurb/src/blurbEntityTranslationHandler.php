<?php

namespace Drupal\rw_blurb;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for blurb_entity.
 */
class blurbEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
