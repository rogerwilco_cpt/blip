<?php

namespace Drupal\rw_blurb\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Blurb entity type entities.
 */
interface blurbEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
