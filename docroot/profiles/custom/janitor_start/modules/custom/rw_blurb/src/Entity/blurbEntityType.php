<?php

namespace Drupal\rw_blurb\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Blurb entity type entity.
 *
 * @ConfigEntityType(
 *   id = "blurb_entity_type",
 *   label = @Translation("Blurb entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\rw_blurb\blurbEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\rw_blurb\Form\blurbEntityTypeForm",
 *       "edit" = "Drupal\rw_blurb\Form\blurbEntityTypeForm",
 *       "delete" = "Drupal\rw_blurb\Form\blurbEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\rw_blurb\blurbEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "blurb_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "blurb_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/blurb_entity_type/{blurb_entity_type}",
 *     "add-form" = "/admin/structure/blurb_entity_type/add",
 *     "edit-form" = "/admin/structure/blurb_entity_type/{blurb_entity_type}/edit",
 *     "delete-form" = "/admin/structure/blurb_entity_type/{blurb_entity_type}/delete",
 *     "collection" = "/admin/structure/blurb_entity_type"
 *   }
 * )
 */
class blurbEntityType extends ConfigEntityBundleBase implements blurbEntityTypeInterface {

  /**
   * The Blurb entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Blurb entity type label.
   *
   * @var string
   */
  protected $label;

}
