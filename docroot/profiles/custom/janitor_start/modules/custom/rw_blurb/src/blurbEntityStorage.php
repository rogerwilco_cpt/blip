<?php

namespace Drupal\rw_blurb;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\rw_blurb\Entity\blurbEntityInterface;

/**
 * Defines the storage handler class for Blurb entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Blurb entity entities.
 *
 * @ingroup rw_blurb
 */
class blurbEntityStorage extends SqlContentEntityStorage implements blurbEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(blurbEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {blurb_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {blurb_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(blurbEntityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {blurb_entity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('blurb_entity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
