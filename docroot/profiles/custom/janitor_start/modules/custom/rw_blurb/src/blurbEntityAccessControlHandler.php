<?php

namespace Drupal\rw_blurb;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Blurb entity entity.
 *
 * @see \Drupal\rw_blurb\Entity\blurbEntity.
 */
class blurbEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\rw_blurb\Entity\blurbEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished blurb entity entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published blurb entity entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit blurb entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete blurb entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add blurb entity entities');
  }

}
