<?php

namespace Drupal\rw_janitor\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class ErrorPageController.
 */
class ErrorPageController extends ControllerBase {

  /**
   * Generate the error page for a page that a user does not have access to
   *
   * @return array $error
   *   Render array of the error message area
   */
  public function build_403() {
    return [
      '#theme'   => 'error_page__403',
      '#message' => $this->t('You do not have permission to access this page'),
    ];
  }
  /**
   * Generate the error page for a missing or incorrect route/url
   *
   * @return array $error
   *   Render array of the error message area
   */
  public function build_404() {
    return [
      '#theme'   => 'error_page__404',
      '#message' => $this->t('The page you are looking cannot be found.'),
    ];
  }
}
