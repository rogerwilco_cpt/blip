<?php

namespace Drupal\am_reports\Controller;

use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Controller\ControllerBase;

/**
 * Class ReportsController.
 */
class ReportsController extends ControllerBase {

  /**
   * Dashboard.
   *
   * @return mixed
   *   Return the dashboard blocks.
   */
  public function dashboard() {
    $page = [];

    $page['dashboard'] = [
      '#theme'              => 'item_list',
      '#type'               => 'ul',
      '#title'              => NULL,
      '#wrapper_attributes' => [
        'class'             => ['am-reports'],
      ],
      '#attributes'         => [
        'class'             => ['am-reports--grid'],
      ],
      '#items'              => [],
    ];

    /** Campaigns */
    if (\Drupal::moduleHandler()->moduleExists('rw_campaigns')) {
      $campaign_progress_summary = views_embed_view('ama_campaigns', 'bk_reports_campaign_progress_summary');
      if ($campaign_progress_summary) {
        // Fetch all the campaign node IDs and titles
        $page['dashboard']['#items']['campaigns_active'] = $campaign_progress_summary;
        $page['dashboard']['#items']['campaigns_active']['#prefix'] = '<h2 class="am-reports--widget-title">' . $this->t('Campaign progress') . '</h2>';
        $page['dashboard']['#items']['campaigns_active']['#wrapper_atributes'] = [
          'class'            => ['am-reports--widget'],
        ];
      }
    }

    /** Donations */
    if (\Drupal::moduleHandler()->moduleExists('webform_donation')) {
      $donation_summary = views_embed_view('am_reports', 'bk_reports_recent_donations');

      if ($donation_summary) {
        $page['dashboard']['#items']['recent_donations'] = $donation_summary;
        $page['dashboard']['#items']['recent_donations']['#prefix'] = '<h2 class="am-reports--widget-title">' . $this->t('Donations') . '</h2>';
        $page['dashboard']['#items']['recent_donations']['#wrapper_attributes'] = [
          'class' => ['am-reports--widget'],
        ];
      }
    }

    $page['dashboard']['#attached']['library'][] = 'am_reports/dashboard';

    // ksm($page);
    return $page;
  }

  private function fetch_campaigns() {
    $campaigns = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadByProperties([
        'type' => 'campaign',
      ]);

    return $campaigns;
  }
}
