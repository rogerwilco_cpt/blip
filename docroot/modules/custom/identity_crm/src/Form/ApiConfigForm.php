<?php

namespace Drupal\identity_crm\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ApiConfigForm.
 */
class ApiConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'identity_crm.apiconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'identity_crm_api_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('identity_crm.apiconfig');
    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Token'),
      '#description' => $this->t('API Token provided by Identity CRM Admin'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('api_token'),
      '#required' => TRUE,
    ];
    $form['api_url_for_integration'] = [
      '#type' => 'url',
      '#title' => $this->t('Api URL For Integration'),
      '#default_value' => $config->get('api_url_for_integration'),
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('identity_crm.apiconfig')
      ->set('api_token', $form_state->getValue('api_token'))
      ->set('api_url_for_integration', $form_state->getValue('api_url_for_integration'))
      ->save();
  }

}
