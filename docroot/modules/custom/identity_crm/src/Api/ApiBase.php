<?php

namespace Drupal\identity_crm\Api;

class ApiBase {

  private $api_token;

  private $url;

  private $data;

  private $client;

  public function __construct($endpoint) {
    $config = \Drupal::config('identity_crm.apiconfig');
    if (empty($config->get('api_token')) || empty($config->get('api_url_for_integration'))) {
      return FALSE;
    }
    else {
      $this->api_token = $config->get('api_token');
      $this->client = \Drupal::httpClient();
      $this->url = $config->get('api_url_for_integration') . $endpoint;
    }

    return TRUE;
  }

  public function setData($data) {
    $this->data['api_token'] = $this->api_token;
    $this->data = array_merge($this->data, $data);
  }

  public function sendRequest($debug = FALSE) {
    try {
      $body = json_encode($this->data);

//      echo '<pre>';
//      echo $body;
//      echo '</pre>';
//
//      die();

      $this->log(true, "Sending API Request To Identity | $body");

      $cSession = curl_init();
      $headers = [];
//      $auth = base64_encode($this->api_username . ':' . $this->api_key);
//      $headers[] = 'Authorization: Basic ' . $auth;
      curl_setopt($cSession, CURLOPT_URL, $this->url);
      curl_setopt($cSession, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($cSession, CURLOPT_HEADER, true);
      curl_setopt($cSession, CURLOPT_CUSTOMREQUEST, 'POST');
      curl_setopt($cSession, CURLOPT_POSTFIELDS, $body);
      $headers[] = 'Content-Type: application/json';
      curl_setopt($cSession, CURLOPT_HTTPHEADER, $headers);
      $response = curl_exec($cSession);
      $httpcode = curl_getinfo($cSession, CURLINFO_HTTP_CODE);

      $response = json_decode($response, TRUE);
      curl_close($cSession);

      $this->log(true, "Identity Response | " . $httpcode);
      if ($httpcode !== 200) {
        return FALSE;
      }
      else {
        return TRUE;
      }
    } catch (\Exception $e) {
      \Drupal::logger('identity_crm')->error($e->getMessage());
      return FALSE;
    }
  }

  public function checkErrors($response, $body) {
    if (array_key_exists('error', $response)) {
      \Drupal::logger('identity_crm')
        ->error($response . " | Message - " . $body);
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  public function log($debug, $message) {
    if ($debug) {
      \Drupal::logger('identity_crm')->debug($message);
    }
  }
}