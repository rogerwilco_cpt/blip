<?php

namespace Drupal\identity_crm\Plugin\WebformHandler;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\WebformTokenManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\identity_crm\Api\ApiBase;

/**
 * Webform example handler.
 *
 * @WebformHandler(
 *   id = "identity_crm_handler",
 *   label = @Translation("Identity CRM Integration"),
 *   category = @Translation("Identity CRM"),
 *   description = @Translation("Configure the webform to integrate with
 *   Identity's internal systems."), cardinality =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_IGNORED,
 *   submission =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */
class IdentityWebformHandler extends WebformHandlerBase {

  /**
   * The token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $tokenManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, WebformSubmissionConditionsValidatorInterface $conditions_validator, WebformTokenManagerInterface $token_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger_factory, $config_factory, $entity_type_manager, $conditions_validator);
    $this->tokenManager = $token_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('webform_submission.conditions_validator'),
      $container->get('webform.token_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'debug' => FALSE,
      'endpoint' => '/api/member_actions/create',
      'field_map' => 'test,test,TRUE',
      'email' => 'siteadmins@rogerwilco.co.za',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['api_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API Integration Settings'),
    ];
    $form['api_settings']['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email addresses for failed submissions'),
      '#description' => $this->t('These email addresses will receive the submission via email when the submission failed. Comma Seperated.'),
      '#default_value' => $this->configuration['email'],
      '#required' => TRUE,
    ];
    $form['api_settings']['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#description' => $this->t('Endpoint to send the values to.'),
      '#default_value' => $this->configuration['endpoint'],
      '#required' => TRUE,
    ];
    $form['api_settings']['field_map'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Fieldmap'),
      '#description' => $this->t('Format: APIField,WebformField/DefaultValue,IsDefaultValue,IsINT. One per line.'),
      '#default_value' => $this->configuration['field_map'],
      '#required' => TRUE,
    ];

    // Development.
    $form['development'] = [
      '#type' => 'details',
      '#title' => $this->t('Development settings'),
    ];
    $form['development']['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debugging'),
      '#description' => $this->t('If checked, every handler method invoked will be displayed onscreen to all users.'),
      '#return_value' => TRUE,
      '#default_value' => $this->configuration['debug'],
    ];

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['endpoint'] = $form_state->getValue('endpoint');
    $this->configuration['email'] = $form_state->getValue('email');
    $this->configuration['field_map'] = $form_state->getValue('field_map');
    $this->configuration['debug'] = (bool) $form_state->getValue('debug');
  }

  /**
   * {@inheritdoc}
   */
  public function alterElements(array &$elements, WebformInterface $webform) {
    $this->debug(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function overrideSettings(array &$settings, WebformSubmissionInterface $webform_submission) {
    $this->debug(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function alterForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $this->debug(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $this->debug(__FUNCTION__);
    if ($value = $form_state->getValue('element')) {
      $form_state->setErrorByName('element', $this->t('The element must be empty. You entered %value.', ['%value' => $value]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $this->debug(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $this->debug(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function preCreate(array &$values) {
    $this->debug(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function postCreate(WebformSubmissionInterface $webform_submission) {
    $this->debug(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function postLoad(WebformSubmissionInterface $webform_submission) {
    $this->debug(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function preDelete(WebformSubmissionInterface $webform_submission) {
    $this->debug(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function postDelete(WebformSubmissionInterface $webform_submission) {
    $this->debug(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(WebformSubmissionInterface $webform_submission) {
    $this->debug(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    $api = new ApiBase($this->configuration['endpoint']);
    if (!$api) {
      \Drupal::logger('identity_crm')
        ->error("Identity API has not been configured yet.");
    }
    else {
      $fields = $this->mapFields($webform_submission->getData(),
        $this->getConfigFields(), $webform_submission);

      $data = $this->buildData($fields);
      $api->setData($data);
      if (!$api->sendRequest($this->configuration['debug'])) {
        $this->sendErrorEmail($this->configuration['email'], json_encode($webform_submission->getData()));
      }
    }
  }

  public function buildData($fields) {
    $data = [];

//    $data['external_id'] = (string) $fields['external_id'];
    $data['action_type'] = 'donation';
    $data['action_name'] = 'Website Donation';

    $data['cons_hash'] = [
      'firstname' => $fields['firstname'],
      'lastname' => $fields['lastname'],
      'emails' => [
        ['email' => $fields['email']],
      ],
      'custom_fields' => [
        [
          'name' => 'donation_id',
          'value' => (string) $fields['donation_id'],
        ],
        [
          'name' => 'donation_type',
          'value' => $fields['donation_type'],
        ],
        [
          'name' => 'donation_amount',
          'value' => (string) $fields['donation_amount'],
        ],
        [
          'name' => 'payment_type',
          'value' => $fields['payment_type'],
        ],
        [
          'name' => 'campaign',
          'value' => $fields['campaign'],
        ],
        [
          'name' => 'bank_name',
          'value' => $fields['bank_name'],
        ],
        [
          'name' => 'account_holder',
          'value' => $fields['account_holder'],
        ],
        [
          'name' => 'account_number',
          'value' => $fields['account_number'],
        ],
        [
          'name' => 'payment_day',
          'value' => $fields['payment_day'],
        ],
      ],
    ];

//    $data['source'] = [
//      'source' => 'website',
//      'medium' => 'donation_form',
//      'campaign' => $fields['campaign'],
//    ];

    return $data;
  }

  public function mapFields($data, $fields, $webform_submission) {
    foreach ($fields as $key => $field) {
      if ($field['default']) {
        if ($field['int']) {
          $return_fields[$key] = (int) $this->tokenManager->replace($field['value'], $webform_submission);
        }
        else {
          $return_fields[$key] = $this->tokenManager->replace($field['value'], $webform_submission);
        }
      }
      else {
        $return_fields[$key] = $data[$field['value']];
      }
    }
    return $return_fields;
  }

  public function getConfigFields() {
    $fields = explode(PHP_EOL, $this->configuration['field_map']);
    foreach ($fields as $field) {
      $line = explode(',', $field);
      $return_fields[trim($line[0])]['default'] = (trim($line[2]) == 'TRUE') ? TRUE : FALSE;
      $return_fields[trim($line[0])]['value'] = trim($line[1]);
      if (array_key_exists(3, $line)) {
        $return_fields[trim($line[0])]['int'] = (trim($line[3]) == 'TRUE') ? TRUE : FALSE;
      }
    }
    return $return_fields;
  }

  public function sendErrorEmail($to, $body) {
    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'identity_crm';
    $key = 'identity_crm_error_email';
    $params['message'] = $body;
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = TRUE;
    $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    if ($result['result'] !== TRUE) {
      $site_name = \Drupal::config('system.site')->get('name');
      \Drupal::logger('identity_crm')
        ->error("There was a problem sending your email to $to");
      drupal_set_message(t("There was a problem sending your request. Please contact $site_name via other means."), 'error');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preprocessConfirmation(array &$variables) {
    $this->debug(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function createHandler() {
    $this->debug(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function updateHandler() {
    $this->debug(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteHandler() {
    $this->debug(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function createElement($key, array $element) {
    $this->debug(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function updateElement($key, array $element, array $original_element) {
    $this->debug(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteElement($key, array $element) {
    $this->debug(__FUNCTION__);
  }

  /**
   * Display the invoked plugin method to end user.
   *
   * @param string $method_name
   *   The invoked method name.
   * @param string $context1
   *   Additional parameter passed to the invoked method name.
   */
  protected function debug($method_name, $context1 = NULL) {
    if (!empty($this->configuration['debug'])) {
      $t_args = [
        '@id' => $this->getHandlerId(),
        '@class_name' => get_class($this),
        '@method_name' => $method_name,
        '@context1' => $context1,
      ];
      $this->messenger()
        ->addWarning($this->t('Invoked @id: @class_name:@method_name @context1', $t_args), TRUE);
    }
  }

}